﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TempObjBehaviour : MonoBehaviour {

    private Rigidbody rb;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
        StartCoroutine(ObjBehaviour());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator ObjBehaviour()
    {
        float a1 = Random.Range(0.5f, 2.5f);
        yield return new WaitForSeconds(a1);
        float a = Random.Range(-0.5f, 0.5f);
        float b = Random.Range(-0.5f, 0.5f);
        float c = Random.Range(100, 400f);
        rb.AddExplosionForce(c, transform.position + new Vector3(a, -0.5f, b), 1000);
        StartCoroutine(ObjBehaviour());
    }
}
