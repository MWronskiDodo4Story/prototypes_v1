﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShootingControls : MonoBehaviour {

    [SerializeField]
    private Transform rightProbe;
    [SerializeField]
    private Transform leftProbe;
    [SerializeField]
    private Transform reflectionProbe;
    [SerializeField]
    private BulletController bullet;
    [SerializeField]
    private Transform crosshair;
    [SerializeField]
    private Transform mainCamera;

    public float cooldownTime;
    public float shootingShakeStrength;
    public float shootingShakeFrequency;

    private bool canFire=true;
    private bool leftCannon = false;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if(GameplayController.Instance.isGameStarted())
        {
            CheckIfShooting();
        }
        
	}
    void CheckIfShooting()
    {
        if (Input.GetAxis("PrimaryFire")<-0.1f)
        {
            Shoot(Mathf.Abs(Input.GetAxis("PrimaryFire")));
        }
    }
    void Shoot(float firePower)
    {
        if (canFire)
        {
            Transform probe=null;
            PickCannon(probe);
            canFire = false;
            CameraShake.Instance.ShakeCamera(shootingShakeStrength*firePower, 0.1f, shootingShakeFrequency, false);
            StartCoroutine(Cooldown(firePower));
        }

    }
    void PickCannon(Transform probe)
    {
        probe = leftCannon ? leftProbe : rightProbe;
        if (leftCannon)
        {
            leftCannon = false;
        }
        else
        {
            leftCannon = true;
        }
        SpawnBullet(probe);
    }
    void SpawnBullet(Transform probe)
    {
        BulletController bc = Instantiate(bullet, probe.position, probe.rotation);
        bc.transform.rotation = crosshair.rotation;
        bc.transform.Rotate(Vector3.up, 180);
        
        Vector3 crossHairChanged = new Vector3(crosshair.position.x, 0, crosshair.position.z);
        Vector3 cameraChanged = new Vector3(mainCamera.position.x, 0, mainCamera.position.z);
        float distance = Vector3.Magnitude(crossHairChanged - cameraChanged);
        float height = crosshair.position.y - mainCamera.position.y;
        float atan = Mathf.Atan2(height, distance);
        float dgr = Mathf.Rad2Deg * atan;
        bc.transform.Rotate(Vector3.left, dgr);

        RaycastHit hit;
        probe.transform.Rotate(Vector3.left, dgr);
        Vector3 fwd = reflectionProbe.transform.TransformDirection(Vector3.forward);
        if (Physics.Raycast(reflectionProbe.transform.position, fwd, out hit, 50))
        {
            if (hit.transform.tag == "Building" || hit.transform.tag == "Enemy")
            {
                Vector3 point = hit.point;
                bc.transform.LookAt(hit.point);
            }
        }
        bc.Move();
    }
    IEnumerator Cooldown(float power)
    {
        yield return new WaitForSeconds(cooldownTime/power);
        canFire = true;

    }
}
