﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameplayController : MonoBehaviour
{

    public int maxHP;
    public float comboDuration;
    public bool skipIntro;
    public bool infiniteHealth;
    public float maxTime;
    private int hp;
    private int points;
    private int screenState = 0;
    private Coroutine currentCombo;
    private int currentComboMultiplier = 1;
    public int maxComboMultiplier = 5;
    private bool gameStarted = false;
    private bool timeWarningSent = false;
    private float startTime;
    private bool gameover = false;
    public Material altCrosshairMat;
    public Material mainCrosshairMat;


    public static GameplayController Instance;

    [SerializeField]
    private Transform mainCamera;
    [SerializeField]
    private Transform reflectionProbe;
    [SerializeField]
    private Transform crosshair;
    [SerializeField]
    private Transform gameOverScreen;

    private void Awake()
    {
        Instance = this;
    }
    void Start()
    {

    }
    private void Update()
    {
        CheckTime();
        if(gameover&&Input.GetKeyDown(KeyCode.Joystick1Button7))
        {
            PlayAgain();
        }
    }
    void CheckTime()
    {
        if((maxTime-(Time.time-startTime))<300&&!timeWarningSent&&gameStarted)
        {
            MessageDisplay.Instance.ShowTextPulse("5 minutes left!", 100, 3);
            timeWarningSent = true;
        }
        if((maxTime - (Time.time - startTime))<=0&&gameStarted)
        {
            GameOver();
        }
    }
    public void GameOver()
    {
        MessageDisplay.Instance.ShowTextNoFadeout("Your score: " + points + " points!", Color.white);
        crosshair.GetComponent<SpriteRenderer>().material = altCrosshairMat;
        Debug.Log("GameOver");
        MessageDisplay.Instance.ShowText("");
        gameover = true;
        EnemySpawner.Instance.DestroyAllSpaceship();
        gameOverScreen.gameObject.SetActive(true);
        gameStarted = false;
        //crosshair.gameObject.SetActive(false);
        LightController.Instance.DisableAllLight();

    }
    public void PlayAgain()
    {
        MessageDisplay.Instance.HideAllText();
        if (skipIntro)
        {
            StartGame();
        }
        else
        {
            reflectionProbe.GetComponent<Animator>().SetTrigger("StartIntroAnimation");
        }
        StartCoroutine(WaitUntilPlayAgain());
    }
    IEnumerator WaitUntilPlayAgain()
    {
        yield return new WaitForSeconds(0.2f);
        gameover = false;
        startTime = Time.time;
        gameOverScreen.gameObject.SetActive(false);
        LightController.Instance.EnableAllLight();
    }
    public void StartGame()
    {
        timeWarningSent = false;
        crosshair.GetComponent<SpriteRenderer>().material = mainCrosshairMat;
        startTime = Time.time;
        gameStarted = true;
        hp = maxHP;
        points = 0;
        crosshair.gameObject.SetActive(true);
    }
    public bool isGameStarted()
    {
        return gameStarted;
    }
    public void AddPoints(int a)
    {
        points += a * currentComboMultiplier;
        if (currentCombo != null)
        {
            StopCoroutine(currentCombo);
        }
        currentCombo = StartCoroutine(Combo());
        if (currentComboMultiplier < maxComboMultiplier)
        {
            currentComboMultiplier++;
        }
    }
    public int GetPoints()
    {
        return points;
    }
    public float GetTime()
    {
        return Time.time - startTime;
    }
    public int GetCurrentComboMultiplier()
    {
        return currentComboMultiplier;
    }
    IEnumerator Combo()
    {

        yield return new WaitForSeconds(comboDuration);
        currentCombo = null;
        currentComboMultiplier = 1;
    }
    public void BreakCombo()
    {
        if (currentCombo != null)
        {
            StopCoroutine(currentCombo);
        }
        currentComboMultiplier = 1;
    }
    public void ChangeHP(int a)
    {
        if(!infiniteHealth)
        {
            hp += a;
            int percentage = (int)((hp * 1f / maxHP * 1f) * 100);
            HealthDisplay.Instance.ShowText(percentage + "% health");
            if (percentage <= 10)
            {
                MessageDisplay.Instance.ShowTextPulse("Critical health");
            }
            if (percentage <= 0)
            {
                GameOver();
            }
        }
        
    }
    public Vector3 GetMainCameraPosition()
    {
        return mainCamera.position;
    }
    public Quaternion GetMainCameraRotation()
    {
        return mainCamera.rotation;
    }
    public Transform GetProbe()
    {
        return reflectionProbe;
    }
    public Vector3 GetProbePosition()
    {
        return reflectionProbe.position;
    }
    public Quaternion GetProbeRotation()
    {
        return reflectionProbe.rotation;
    }
    public int GetScreenState()
    {
        return screenState;
    }
    public void SetScreenState(float value)
    {

        if (value > 315 || value <= 45)
        {
            screenState = 0;
        }
        if (value > 45 && value <= 135)
        {
            screenState = 1;
        }

        if (value > 135 && value <= 225)
        {
            screenState = 2;
        }
        if (value > 225 && value <= 315)
        {
            screenState = 3;
        }
    }
}
