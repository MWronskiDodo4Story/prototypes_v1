﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BlimpRocketController : MonoBehaviour {

    public Transform explosion;
    [Range(1, 500)]
    public float distance;
    [Range(1, 500)]
    public float speed;
    [SerializeField]
    private ParticleSystem ps;
    [SerializeField]
    private TextMeshPro floatingText;
    [SerializeField]
    private MeshRenderer meshRenderer;

    private bool blimpSpawned = false;
    private BlimpController blimpToFollow = null;
    private Transform probe;
    private bool rightDir;
    private ConstantForce force;
    private Rigidbody rb;
    private bool moving = false;
    private bool movingToBlimp = false;
    private float savedBlimpAngle;
    private float travelledAngles;
    private bool playerRocket = false;
    // Use this for initialization
    void Start ()
    {
        probe = GameplayController.Instance.GetProbe();
        rb = GetComponent<Rigidbody>();
        force = GetComponent<ConstantForce>();
        StartCoroutine(BeginMovementAfterSeconds(4));
	}
	
	// Update is called once per frame
	void Update () {
        
        if (moving)
        {
            UpdatePosition();
            rb.AddForce(transform.forward * speed, ForceMode.Acceleration);
           
        } 
        if(blimpToFollow!=null)
        {
            transform.LookAt(blimpToFollow.rend.bounds.center);
        }
    }
    public void SetMaterial(Material mat)
    {
        meshRenderer.material = mat;
    }
    public void SetPlayerRocket()
    {
        playerRocket = true;
    }
    private void LateUpdate()
    {
        if(blimpSpawned)
        {
            return;
        }
        float startRot = transform.eulerAngles.y;
        Vector3 cross = (transform.position - probe.position).normalized;
        Vector3 heading = cross;

        if (rightDir)
        {
            transform.rotation = Quaternion.LookRotation(-heading, Vector3.up);
            transform.eulerAngles += new Vector3(-transform.eulerAngles.x, -90, 0);
        }
        else
        {
            transform.rotation = Quaternion.LookRotation(heading, Vector3.up);
            transform.eulerAngles += new Vector3(-transform.eulerAngles.x, -90, 0);
        }
        if(blimpSpawned)
        {
            return;
        }
        float endRot = transform.eulerAngles.y;
        float diff = Mathf.Abs(endRot - startRot);
        if(diff>300)
        {
            diff -= 360;
        }
        travelledAngles += diff;
        if(travelledAngles>=360)
        {
            moving = false;
            TryToSpawnBlimp();
            blimpSpawned = true;
            travelledAngles = 0;
        }
    }
    public void ChangeRotation()
    {
        rightDir = true;
    }
    void UpdatePosition()
    {
        probe.LookAt(transform);
        Vector3 placement = probe.position + (probe.forward * distance);
        transform.position = placement;
    }
    void BeginMovement()
    {
        force.force = Vector3.zero;
        moving = true;
        travelledAngles = 0;
        ps.Play();
    }
    IEnumerator BeginMovementAfterSeconds(float time)
    {
        yield return new WaitForSeconds(time);
        BeginMovement();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Plane")
        {
            SpawnFloatingText(other.transform);
            GameplayController.Instance.AddPoints(250);
            BlimpSpawner.Instance.RemoveRocket(this);
            Destroy(this.gameObject);
        }
        if(blimpToFollow!=null)
        {
            if(other.GetComponent<BlimpController>()==blimpToFollow)
            {
                blimpToFollow.GetComponent<BlimpController>().SetActiveAfterSeconds();
                Die();
            }
        }
    }
    void SpawnFloatingText(Transform tr)
    {
        TextMeshPro tmp = Instantiate(floatingText, tr);
        tmp.fontSize = 35;
        tmp.text = 250 * GameplayController.Instance.GetCurrentComboMultiplier() + "";
        Vector3 localpos = tmp.transform.position;
        tmp.transform.SetParent(transform.parent, false);
        tmp.transform.position = localpos;
        Vector3 newPos = Vector3.MoveTowards(GameplayController.Instance.GetProbePosition(), localpos, 100);
        tmp.transform.position = newPos;
        tmp.transform.rotation = GameplayController.Instance.GetMainCameraRotation();
        tmp.transform.Rotate(tmp.transform.up, 180);
    }
    void TryToSpawnBlimp()
    {
        rb.velocity = Vector3.zero;
        BlimpController blimp =  BlimpSpawner.Instance.SpawnBlimp(savedBlimpAngle);
        blimpToFollow = blimp;
        transform.LookAt(blimpToFollow.rend.bounds.center);
        rb.AddForce(transform.forward * speed, ForceMode.VelocityChange);
    }
    void Die()
    {
        Transform tr = Instantiate(explosion, transform.position, transform.rotation);
        BlimpSpawner.Instance.RemoveRocket(this);
        Destroy(this.gameObject);
    }
    public float GetBlimpAngle()
    {
        return savedBlimpAngle;
    }
    public void SetBlimpAngle(float angle)
    {
        savedBlimpAngle = angle;
    }
}
