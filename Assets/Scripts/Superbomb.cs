﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Superbomb : MonoBehaviour {

    public List<Transform> bombs;

    private void Start()
    {
        StartCoroutine(Explode());
    }
    IEnumerator Explode()
    {
        for(int i=0; i<bombs.Count; i++)
        {
            int rand = Random.Range(0, bombs.Count);
            bombs[rand].gameObject.SetActive(true);
            yield return new WaitForSeconds(0.15f);
        }
    }
}
