﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour
{
    public float deathTime = 5f;
    [SerializeField]
    private Transform explosion;
    // Use this for initialization
    void Start()
    {
        StartCoroutine(DieAfterSeconds(deathTime));

    }

    // Update is called once per frame
    void Update()
    {

    }
    public void GetHit()
    {

    }
    void Die()
    {
        Destroy(this.gameObject);
    }
    IEnumerator DieAfterSeconds(float time)
    {
        yield return new WaitForSeconds(time);
        Die();
    }
    public void Move()
    {
        GetComponent<Rigidbody>().AddForce(transform.forward * 10000f);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            
            Transform newExplosion = Instantiate(explosion, other.transform);
            newExplosion.position = transform.position;
            if (other.gameObject.GetComponent<EnemyController>())
            {
                other.gameObject.GetComponent<EnemyController>().TakeDamage(1);
            }
        }
        Die();
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Enemy")
        {
            
            Transform newExplosion = Instantiate(explosion, collision.transform);
            newExplosion.position = transform.position;
            if(collision.gameObject.GetComponent<EnemyController>())
            {
                collision.gameObject.GetComponent<EnemyController>().TakeDamage(1);
            }
        }
        Die();
    }

}
