﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PlaneController : MonoBehaviour {


    private Coroutine rotationCoroutine = null;
    private bool canSteer = true;
    private bool rotating = false;
    bool rightDir = true;
    bool planeFlyingRight = true;
    private Rigidbody rb;
    private ConstantForce gravity;
    private float initialGravity;
    private float initialGravityBreak;
    private Vector3 currentPos = Vector3.zero;
    private Vector3 previousPos = Vector3.zero;
    private Animator mainAnim;
    private Vector3 startPos;
    private Quaternion startRot;
    private bool moving = false;

    [SerializeField]
    private Animator anim;
    [SerializeField]
    private MeshRenderer meshRenderer;
    [SerializeField]
    private Transform propeller;
    [SerializeField]
    private Transform probe;
    [SerializeField]
    private ParticleSystem smoke;

    public ParticleSystem thrusters;
    public float timeTillRotation;
    public float rotationSpeed;
    public float gravityForce;
    public float gravityBreakTreshold;
    [Range(1, 500)]
    public float acceleration;
    [Range(1, 500)]
    public float distance;
    [Range(0f, 1f)]
    public float sensitivity;
    [Range(0f, 1f)]
    public float leftRightSensitivity;
    public float movementDelay;

    // Use this for initialization
    void Start () {
        initialGravityBreak = gravityBreakTreshold;
        initialGravity = gravityForce;
        startPos = transform.position;
        startRot = transform.rotation;
        currentPos = transform.position;
        previousPos = transform.position;
        rb = GetComponent<Rigidbody>();
        gravity = GetComponent<ConstantForce>();
        rb.centerOfMass = rb.centerOfMass - transform.up*(1);
        StartCoroutine(BeginMovement());
    }
    private void Reset()
    {
        gravityForce = initialGravity;
        rb.isKinematic = false;
        smoke.gameObject.SetActive(false);
        GameplayController.Instance.BreakCombo();
        StartCoroutine(Blink(2, 0.2f));
        transform.position = startPos;
        transform.rotation = startRot;
        StartCoroutine(BeginMovement());
    }
    IEnumerator WaitForReset()
    {
        yield return new WaitForSeconds(3);
        Reset();
    }

    // Update is called once per frame
    void FixedUpdate () {
        
        if(canSteer)
        {
            PropellForward();
        }
        if(moving)
        {
            ChangeGravityBasedOnHeight();
            UpdateGravity();
        }
        
        UpdatePosition();
        
    }
    private void LateUpdate()
    {
        if (rightDir)
        {
            transform.localEulerAngles += new Vector3(0, -transform.localEulerAngles.y + SceneMovement.Instance.GetCurrentRotationEuler().y + 90, -transform.localEulerAngles.z);
        }
        else
        {
            transform.localEulerAngles += new Vector3(0, -transform.localEulerAngles.y + SceneMovement.Instance.GetCurrentRotationEuler().y - 90, -transform.localEulerAngles.z + 180);
        }
    }
    IEnumerator BeginMovement()
    {
        moving = false;
        yield return new WaitForSeconds(movementDelay);
        moving = true;
    }
    IEnumerator Blink(float time, float frequency)
    {
        float startTime = Time.time;
        while(startTime+time>Time.time)
        {
            if(meshRenderer.enabled)
            {
                meshRenderer.enabled = false;
            }
            else
            {
                meshRenderer.enabled = true;
            }
            yield return new WaitForSeconds(frequency);
        }
        meshRenderer.enabled = true;
    }
    void AdjustSmoke()
    {
        float magnitude = rb.velocity.magnitude;
        if (magnitude > 5)
        {
            smoke.gameObject.SetActive(true);
        }
        else
            smoke.gameObject.SetActive(false);
    }
    void UpdatePosition()
    {
        probe.LookAt(transform);
        Vector3 placement = probe.position+(probe.forward * distance);
        transform.position = placement;
        
    }
    void PropellForward()
    {
        if(moving)
        {
            LeftRightMovement();
            float movement = (currentPos - previousPos).magnitude;
            UpDownMovement();
            Acceleration();
            ConstantAcceleration();
        }
    }
    private void UpDownMovement()
    {
        if (rotating)
        {
            return;
        }
        if (Input.GetAxis("PadLeftStickY") > sensitivity || Input.GetAxis("PadLeftStickY") < -sensitivity)
        {
            if ((rightDir && planeFlyingRight) || (!rightDir && !planeFlyingRight))
            {
                transform.localEulerAngles += new Vector3(-Input.GetAxis("PadLeftStickY") * rotationSpeed, 0, 0);
            }
            else
            {
                transform.localEulerAngles += new Vector3(Input.GetAxis("PadLeftStickY") * rotationSpeed, 0, 0);
            }
        }
    }

    private void LeftRightMovement()
    {
        if (Input.GetAxis("PadLeftStickX") > leftRightSensitivity && planeFlyingRight)
        {
            if ((int)transform.eulerAngles.x != 0)
            {

                if (transform.eulerAngles.x < 90)
                {
                    transform.eulerAngles -= new Vector3(rotationSpeed * Input.GetAxis("PadLeftStickX"), 0, 0);
                }
                else
                {
                    transform.eulerAngles += new Vector3(rotationSpeed * Input.GetAxis("PadLeftStickX"), 0, 0);

                }
            }
        }
        if (Input.GetAxis("PadLeftStickX") < -leftRightSensitivity && !planeFlyingRight)
        {
            if ((int)transform.eulerAngles.x != 180)
            {
                if (transform.eulerAngles.x >270)
                {
                    transform.eulerAngles -= new Vector3(rotationSpeed * Input.GetAxis("PadLeftStickX") , 0, 0);
                   
                }
                else
                {
                    transform.eulerAngles += new Vector3(rotationSpeed * Input.GetAxis("PadLeftStickX"), 0, 0);

                }
            }
        }
        
        if (rotating)
        {
            return;
        }
        if(Input.GetAxis("PadLeftStickX") > leftRightSensitivity&&!planeFlyingRight|| Input.GetAxis("PadLeftStickX") < -leftRightSensitivity&&planeFlyingRight)
        {
            planeFlyingRight = !planeFlyingRight;

            anim.SetTrigger("RotatePlaneLeft");
        }
    }
    public void RotationStarted()
    {
        rotating = true;
    }

    public void RotationEnded()
    {
        rotating = false;
    }
    private void Acceleration()
    {
        int i = 1;
        if(!planeFlyingRight)
        {
            i = -1;
        }

        if (Input.GetAxis("PrimaryFire") < -0.1f)
        {
            thrusters.Play();
            rb.AddForce(transform.forward * acceleration * Input.GetAxis("PrimaryFire") * i, ForceMode.Acceleration);
        }
        else
        {
            thrusters.Stop();
        }
    }
    private void ConstantAcceleration()
    {
        int i = 1;
        if (!planeFlyingRight)
        {
            i = -1;
        }
        rb.AddForce(-transform.forward * acceleration*i, ForceMode.Force);
    }
    void RotatePlane()
    {
        anim.SetTrigger("RotatePlane");
    }
    IEnumerator BeginFlight(float time)
    {
        float startTime = Time.time;
        while(startTime+time>Time.time)
        {
            rb.AddForce(-transform.forward*acceleration, ForceMode.Acceleration);
            yield return new WaitForFixedUpdate();
        }
        canSteer = true;
    }
    void ChangeGravityBasedOnHeight()
    {
        if(transform.localPosition.y>260)
        {
            gravityForce = initialGravity * 12;
            gravityBreakTreshold = 1000f;
        }
        else
        {
            gravityForce = initialGravity;
            gravityBreakTreshold = initialGravityBreak;
        }
    }
    void UpdateGravity()
    {

        float angle = Vector3.Angle(Vector3.up, transform.forward);
        float normalizedUp = Mathf.Abs((angle - 90) - 90) / (180 - 90);
        
        float velocity = rb.velocity.magnitude;
        if(velocity > gravityBreakTreshold)
        {
            velocity = gravityBreakTreshold;
        }
        if(velocity<gravityBreakTreshold/2)
        {
            velocity = 0;
        }
        float g = ((-gravityForce) - ((-gravityForce) * (velocity / gravityBreakTreshold)*normalizedUp));
        gravity.force = new Vector3(0, g, 0);
        previousPos = transform.position;
    }
    void Crash()
    {
        moving = false;
        gravity.force = new Vector3(0, -100f, 0);
        thrusters.Stop();
        smoke.gameObject.SetActive(true);
        StartCoroutine(WaitForReset());
    }
    private void OnCollisionEnter(Collision collision)
    {
        TerrainCollision(collision);
    }
    void BlimpCollision(Collision collision)
    {
        if (collision.gameObject.tag == "Blimp" && rb.velocity.magnitude > 10)
        {
            RaycastHit hit1 = new RaycastHit();
            Ray ray1 = new Ray(transform.position, transform.forward);
            Physics.Raycast(ray1, out hit1);
            if (hit1.transform != null)
            {
                if (hit1.transform.tag == "Blimp")
                {

                    Crash();
                    return;
                }
            }
            RaycastHit hit2 = new RaycastHit();
            Ray ray2 = new Ray(transform.position, -transform.forward);
            Physics.Raycast(ray2, out hit2);
            if (hit2.transform != null)
            {
                if (hit2.transform.tag == "Blimp")
                {

                    Crash();
                }
            }

        }
    }
    void TerrainCollision(Collision collision)
    {
        if (collision.gameObject.tag == "Terrain" && rb.velocity.magnitude > 5)
        {
            if (Vector3.Dot(transform.forward, Vector3.down) > 0.5f || Vector3.Dot(-transform.forward, Vector3.down) > 0.5f)
            {
                Crash();
            }
        }
    }

}
