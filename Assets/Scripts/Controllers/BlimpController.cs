﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlimpController : MonoBehaviour {

    private bool idle = true;
    private bool hasRocket = false;
    private int rocketDelay = 0;
    private float spawnAngle;
    private int sector;

    public float bombDropDelay;
    public float bombDropFrequency;
    public float bombDropVariance;
    public Material bombPlayerMat;

    [SerializeField]
    public MeshRenderer rend;
    [SerializeField]
    private Transform bomb;
    [SerializeField]
    private BlimpRocketController blimpRocket;
    [SerializeField]
    public Animator anim;

    private int load;

	// Use this for initialization
	void Start () {
        SetLoad();
        StartCoroutine(BombDropEnum());
	}
	
	void Update () {
		
	}
    void SetLoad()
    {
        load = Random.Range(3, 9);
        int i = Random.Range(0, 2);
        if(i==1)
        {
            hasRocket = true;
            rocketDelay = Random.Range(0, load);
        }
    }
    public void SetSector(int a)
    {
        sector = a;
    }
    public void SetSpawnAngle(float f)
    {
        spawnAngle = f;
    }
    public float GetSpawnAngle()
    {
        return spawnAngle;
    }
    void DropBomb()
    {
        Transform newBomb = Instantiate(bomb, this.transform);
        newBomb.parent = transform.parent;
        newBomb.position += transform.up * -2;
        load--;
    }
    void DropRocket()
    {
        BlimpRocketController newRocket = Instantiate(blimpRocket, this.transform);
        newRocket.transform.parent = transform.parent;
        newRocket.transform.position += transform.up * -2;
        int r = Random.Range(0, 2);
        hasRocket = false;
        if (r == 0)
        {
            Debug.Log("Roller a " + r);
            newRocket.transform.eulerAngles += new Vector3(0, 180, 0);
            newRocket.ChangeRotation();
        }
        SetNewRocketBlimpAngle(newRocket, r);
        

        if (sector == SceneMovement.Instance.GetPlayerSector())
        {
            newRocket.SetMaterial(bombPlayerMat);
            newRocket.SetPlayerRocket();
        }
        BlimpSpawner.Instance.AddRocket(newRocket);
    }
    void SetNewRocketBlimpAngle(BlimpRocketController newRocket, int rand)
    {
        float ang = spawnAngle;
        while(!BlimpSpawner.Instance.CanSpawnHere(ang))
        {
            if (rand == 1)
                ang -= 1;
            else
                ang += 1;
        }
        newRocket.SetBlimpAngle(ang);
    }
    void Retreat()
    {
        StartCoroutine(WaitBeforeDestroy());
    }
    void BlimpIdle()
    {
        StartCoroutine(BlimpIdleEnum());
    }
    IEnumerator WaitBeforeDestroy()
    {
        yield return new WaitForSeconds(1);
        anim.SetTrigger("Retreat");
        yield return new WaitForSeconds(4f);
        BlimpSpawner.Instance.RemoveBlimp(this);
        Destroy(this.gameObject);
    }
    int bombDropNo = 0;
    IEnumerator BombDropEnum()
    {
        yield return new WaitForSeconds(bombDropDelay);
        float random = Random.Range(-bombDropFrequency * bombDropVariance, bombDropFrequency * bombDropVariance);
        yield return new WaitForSeconds(bombDropFrequency + random);
        if(bombDropNo==rocketDelay&&hasRocket)
        {
            DropRocket();
        }
        else
        {
            DropBomb();
        }
        
        bombDropNo++;
        if(load>0)
        {
            StartCoroutine(BombDropEnum());
        }
        else
        {
            Retreat();
        }
    }
    public void SetInactive()
    {
        rend.enabled = false;
    }
    public void SetActive()
    {
        rend.enabled = true;
    }
    public void SetActiveAfterSeconds()
    {
        StartCoroutine(SetActiveEnum(1f));
    }
    IEnumerator SetActiveEnum(float time)
    {
        yield return new WaitForSeconds(time);
        rend.enabled = true;
    }
    IEnumerator BlimpIdleEnum()
    {
        while (idle)
        {

           
            float movementDuration = Random.Range(2f, 4f);

            float randomX = Random.Range(-3f, 3f);
            float randomY = Random.Range(-3f, 3f);
            float randomZ = Random.Range(-3f, 3f);
            Vector3 randomV = new Vector3(transform.position.x+randomX, transform.position.y + randomY, transform.position.z + randomZ);
            Vector3 startPos = transform.position;
            float startTime = Time.time;
            float distance = (startPos - randomV).magnitude;
            while (startTime + movementDuration > Time.time)
            {
                transform.position = Vector3.MoveTowards(startPos, randomV, distance * ((Time.time - startTime) / movementDuration));
                yield return new WaitForEndOfFrame();
            }
            startTime = Time.time;
            while (startTime + movementDuration > Time.time)
            {
                transform.position = Vector3.MoveTowards(randomV, startPos, distance * ((Time.time - startTime) / movementDuration));
                yield return new WaitForEndOfFrame();
            }
            transform.position = startPos;
        }
    }
}
