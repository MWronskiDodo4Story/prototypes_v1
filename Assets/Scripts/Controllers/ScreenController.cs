﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenController : MonoBehaviour {

    [SerializeField]
    private Transform screen;

    public static ScreenController Instance;

    public Material altMat;

    // Use this for initialization
    void Start () {
        if (Instance == null) Instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UpdateScreenWidth(float f)
    {
        screen.localScale = new Vector3(f/10, screen.localScale.y,  screen.localScale.z);
    }
    public void SetupAnchorOverride(Transform t)
    {
        screen.GetComponent<MeshRenderer>().probeAnchor = t;
    }
    public void UnSmoothScreen()
    {
        screen.GetComponent<MeshRenderer>().material = altMat;
    }
}
