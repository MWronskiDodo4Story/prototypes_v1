﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class BombController : MonoBehaviour {

    [SerializeField]
    private TextMeshPro floatingText;
    [SerializeField]
    private Transform explosion;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Plane")
        {
            SpawnFloatingText(other.transform);
            GameplayController.Instance.AddPoints(100);
            Destroy(this.gameObject);
        }
        if(other.tag == "Terrain"||other.tag=="Building")
        {
            Explode();
        }
    }
    void Explode()
    {
        Instantiate(explosion, transform.position, transform.rotation);
        Destroy(this.gameObject);
    }
    void SpawnFloatingText(Transform tr)
    {
        TextMeshPro tmp = Instantiate(floatingText, tr);
        tmp.fontSize = 30;
        tmp.text = 100 * GameplayController.Instance.GetCurrentComboMultiplier() + "";
        Vector3 localpos = tmp.transform.position;
        tmp.transform.SetParent(transform.parent, false);
        tmp.transform.position = localpos;
        Vector3 newPos = Vector3.MoveTowards(GameplayController.Instance.GetProbePosition(), localpos, 100);
        tmp.transform.position = newPos;
        tmp.transform.rotation = GameplayController.Instance.GetMainCameraRotation();
        tmp.transform.Rotate(tmp.transform.up, 180);

    }
}
