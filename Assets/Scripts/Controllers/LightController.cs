﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightController : MonoBehaviour {

    [SerializeField]
    private Light mainDirectionalLight;
    [SerializeField]
    private Terrain mainTerrain;
    public Material altSkybox;

    public static LightController Instance;

    private bool isEnabled = true;

    private Material mainSkybox;
    private Color ambientColor;
    private float ambientIntensity;
	// Use this for initialization
	void Start () {
        if (Instance == null) Instance = this;
        mainSkybox = RenderSettings.skybox;
        ambientColor = RenderSettings.ambientLight;
        ambientIntensity = RenderSettings.ambientIntensity;
	}
    public void DisableAllLight()
    {
        isEnabled = false;
        mainDirectionalLight.gameObject.SetActive(false);
        RenderSettings.skybox = altSkybox ;
        RenderSettings.ambientLight = Color.black;
        RenderSettings.ambientIntensity = 0;
        mainTerrain.gameObject.SetActive(false);
    }
    public void EnableAllLight()
    {
        isEnabled = true;
        mainDirectionalLight.gameObject.SetActive(true);
        RenderSettings.skybox = mainSkybox;
        RenderSettings.ambientLight = ambientColor;
        RenderSettings.ambientIntensity = ambientIntensity;
        mainTerrain.gameObject.SetActive(true);
    }
    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.Joystick1Button3))
        {
            if (isEnabled)
                DisableAllLight();
            else
                EnableAllLight();
        }
    }

}
