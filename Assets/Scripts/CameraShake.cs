﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraShake : MonoBehaviour {

    [SerializeField]
    private Transform mainCamera;

    public static CameraShake Instance;
    private Vector3 lastCoroutinesInitialPos;
    private bool isPriorityCoroutineRunning = false;
    public bool enableShake = true;
	// Use this for initialization
	void Start () {
		if(Instance==null)
        {
            Instance = this;
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShakeCamera(float strenght, float duration, float shakesPerSecond, bool priority)
    {
        if(!isPriorityCoroutineRunning&&GameplayController.Instance.isGameStarted()&&enableShake)
        {
            StopAllCoroutines();
            if (lastCoroutinesInitialPos != Vector3.zero)
            {
                mainCamera.position = lastCoroutinesInitialPos;
            }
            Vector3 initialPos = mainCamera.position;
            StartCoroutine(ShakeCoroutine(strenght, duration, shakesPerSecond, initialPos, priority));
        }
    }
    IEnumerator ShakeCoroutine(float strength, float duration, float shakesPerSecond, Vector3 initialPos, bool priority)
    {
        if(priority)
        {
            isPriorityCoroutineRunning = true;
        }
        lastCoroutinesInitialPos = initialPos;
        bool shakingLeft = true;
        for(int i=0; i<duration*shakesPerSecond;i++)
        {
            yield return new WaitForSeconds(1 / shakesPerSecond);
            if(shakingLeft)
            {
                mainCamera.position += mainCamera.right * strength;
                shakingLeft = false;
            }
            else
            {
                mainCamera.position += mainCamera.right * (-strength);
                shakingLeft = true;
            }
        }
        mainCamera.position = initialPos;
        isPriorityCoroutineRunning = false;
    }
}
