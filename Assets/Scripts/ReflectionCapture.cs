﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReflectionCapture : MonoBehaviour {

    private ReflectionProbe probe;
    private MeshRenderer rend;
    
	// Use this for initialization
	void Start () {
        probe = GetComponent<ReflectionProbe>();
	}
	void Update () {
        probe.RenderProbe();
	}
}
