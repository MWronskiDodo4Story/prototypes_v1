﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroAnimationEvent : MonoBehaviour {
    

    private void Start()
    {
        if(!GameplayController.Instance.skipIntro)
        {
            GetComponent<Animator>().SetTrigger("StartIntroAnimation");
        }
        else
        {
            GameplayController.Instance.StartGame();
        }
    }

    public void IntroAnimationEnded()
    {
        MessageDisplay.Instance.ShowText("Start!", 200);
        GameplayController.Instance.StartGame();
    }
}
