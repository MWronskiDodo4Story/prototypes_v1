﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EnemyController : MonoBehaviour {

    public int hp = 1;
    private Rigidbody rb;
    private bool alive = true;
    private bool exploded = false;
    public List<Transform> explosion;

    [SerializeField]
    private TextMeshPro floatingText;
    [SerializeField]
    private Transform smoke;
    [SerializeField]
    private Transform smokeLeft;
    [SerializeField]
    private Transform smokeRight;

    public Transform fire;

    public float shakePower;
    public float shakeDuration;
    public float shakeFrequency;
	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {

    }
    public void TakeDamage(int damage)
    {
        if(alive)
        {
            hp = hp - damage;
            if (hp <= 0)
            {
                alive = false;
                Die();
            }
        }
        
    }
    public void DamagePlayer()
    {
        EnemySpawner.Instance.RemoveSpaceship(this);
        CameraShake.Instance.ShakeCamera(shakePower, shakeDuration, shakeFrequency, true);
        GameplayController.Instance.ChangeHP(-5);
        GameplayController.Instance.BreakCombo();
        Destroy(this.gameObject);
    }
    
    void Die()
    {
        SpawnFloatingText();
        PlaySmoke();
        GameplayController.Instance.AddPoints(100);
        MessageDisplay.Instance.ShowText(GameplayController.Instance.GetPoints() + " points", 80);
        gameObject.layer = 8;
        GetComponent<Animator>().enabled = false;
        rb.useGravity = true;
        AddDeathMovement();
        StartCoroutine(DieAfterSeconds(3f));
    }
    void PlaySmoke()
    {
        int r1 = Random.Range(0, 3);
        if (r1 == 2)
        {
            smokeLeft.gameObject.SetActive(true);
        }
        int r2 = Random.Range(0, 3);
        if (r2 == 2)
        {
            smokeRight.gameObject.SetActive(true);
        }
        smoke.gameObject.SetActive(true);
    }
    void SpawnFloatingText()
    {
        TextMeshPro tmp = Instantiate(floatingText, transform);
        tmp.text = 100 * GameplayController.Instance.GetCurrentComboMultiplier()+"";
        Vector3 localpos = tmp.transform.position;
        tmp.transform.SetParent(transform.parent, false);
        tmp.transform.position = localpos;
        Vector3 newPos = Vector3.MoveTowards(GameplayController.Instance.GetProbePosition(), localpos, 40);
        tmp.transform.position = newPos;
        tmp.transform.rotation = GameplayController.Instance.GetMainCameraRotation();
        tmp.transform.Rotate(tmp.transform.up, 180);

    }
    void AddDeathMovement()
    {
        rb.AddForce(-transform.forward*400000f);
        float magnitude = rb.velocity.magnitude;
    }
    void AddDeathExplosionForce()
    {
        float randomPointX = Random.Range(-1f, 1f);
        float randomPointY = Random.Range(-1f, 1f);
        float randomPointZ = Random.Range(-1f, 1f);
        rb.AddExplosionForce(5000f, transform.position + new Vector3(randomPointX, randomPointY, randomPointZ), 100);
    }
    private void OnCollisionEnter(Collision collision)
    {
        
        if (collision.gameObject.tag == "Building")
        {

            Instantiate(fire, collision.contacts[0].point, transform.rotation);
            rb.drag = 1;
            if(alive == false && exploded == false)
            {
                StartCoroutine(Explode());
            }
        }
    }
    IEnumerator Explode()
    {
        exploded = true;
        int times = Random.Range(3, 5);
        for(int i=0; i<times; i++)
        {
            float time = Random.Range(0.2f, 0.6f);
            int which = Random.Range(0, explosion.Count);
            yield return new WaitForSeconds(time);
            explosion[which].gameObject.SetActive(true);
        }
    }
    IEnumerator DieAfterSeconds(float time)
    {
        EnemySpawner.Instance.RemoveSpaceship(this);
        yield return new WaitForSeconds(time);
        Destroy(this.gameObject);
    }
    void Shoot()
    {

    }

}
