﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlimpSpawner : MonoBehaviour {
    

    [SerializeField]
    private BlimpController blimp;
    [SerializeField]
    private Transform probe;
    [SerializeField]
    private Transform scene;

    public float spawnDist;
    public float spawnHeight;
    public float spawnFrequency;
    public float spawnVariance;
    private List<BlimpController> spawnedEnemies;
    private List<BlimpRocketController> activeRockets;


    public static BlimpSpawner Instance;

    private void Start()
    {
        if(Instance==null)
        {
            Instance = this;
        }
        spawnedEnemies = new List<BlimpController>();
        activeRockets = new List<BlimpRocketController>();
        StartCoroutine(SpawnEnemies());
    }
    /*
  _________________________   ___
 /                         \_/   |_
|          GOODYEAR                |
|                                 _|
 \_________________________/ \___|
          |________|
      */
    IEnumerator SpawnEnemies()
    {
        float random = Random.Range(-spawnFrequency * spawnVariance, spawnFrequency * spawnVariance);
        yield return new WaitForSeconds(spawnFrequency + random);

        if(spawnedEnemies.Count<=10)
        {
            SpawnBlimp();
        }
        StartCoroutine(SpawnEnemies());
    }
    public void RemoveBlimp(BlimpController blimp)
    {
        spawnedEnemies.Remove(blimp);
    }
    void SpawnBlimp()
    {
        BlimpController newBlimp = Instantiate(blimp, scene);
        spawnedEnemies.Add(newBlimp);
        newBlimp.transform.position = probe.position;
        float rand = Random.Range(0, 360);
        newBlimp.anim.SetTrigger("Appear");
        while (!CanSpawnHere(rand))
        {
            rand = Random.Range(0, 360);
        }
        SetBlimpPosition(newBlimp, rand);
    }
    public BlimpController SpawnBlimp(float rand)
    {
        BlimpController newBlimp = Instantiate(blimp, scene);
        spawnedEnemies.Add(newBlimp);
        newBlimp.transform.position = probe.position;
        SetBlimpPosition(newBlimp, rand);
        newBlimp.SetInactive();
        return newBlimp;
    }
    public bool CanSpawnHere(float angle)
    {
        foreach (BlimpController blimp in spawnedEnemies)
        {
            if (angle < blimp.GetSpawnAngle() + 15 && angle > blimp.GetSpawnAngle() - 15)
            {
                return false;
            }
        }
        foreach (BlimpRocketController rocket in activeRockets)
        {
            if (angle < rocket.GetBlimpAngle() + 15 && angle > rocket.GetBlimpAngle() - 15)
            {
                return false;
            }
        }
        return true;
    }
    void SetBlimpPosition(BlimpController newBlimp, float rand)
    {
        newBlimp.SetSpawnAngle(rand);
        newBlimp.transform.eulerAngles = new Vector3(0, rand, 0);
        newBlimp.transform.Translate(transform.forward * spawnDist);
        newBlimp.transform.position += new Vector3(0, 18, 0);
        newBlimp.transform.Rotate(newBlimp.transform.up, 90);
        newBlimp.SetSector(SceneMovement.Instance.GetSector(newBlimp.transform));
    }


    public void AddRocket(BlimpRocketController rocket)
    {
        activeRockets.Add(rocket);
    }
    public void RemoveRocket(BlimpRocketController rocket)
    {
        activeRockets.Remove(rocket);
    }

}
