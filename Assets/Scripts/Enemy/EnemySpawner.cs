﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {

    //public List<Animation> firstScreenAnimations = new List<Animation>();
    [Range(0f, 20f)]
    public float spawnTimer;
    [Range(0, 1)]
    public float spawnTimerVariance;


    public static EnemySpawner Instance;

    [SerializeField]
    public EnemyController spaceship;

    private List<EnemyController> spawnedEnemies;

    private void Start()
    {
        if(Instance==null)
        {
            Instance = this;
        }
        spawnedEnemies = new List<EnemyController>();
        StartCoroutine(SpawnEnemies());
    }

    IEnumerator SpawnEnemies()
    {
        float random = Random.Range(-spawnTimer * spawnTimerVariance, spawnTimer * spawnTimerVariance);
        yield return new WaitForSeconds(spawnTimer+random);
        if(GameplayController.Instance.isGameStarted())
        {
            SpawnSpaceship();
        }
        StartCoroutine(SpawnEnemies());
    }
    void SpawnSpaceship()
    {
        EnemyController newSpaceship =  Instantiate(spaceship);
        spawnedEnemies.Add(newSpaceship);
        Animator anim = newSpaceship.GetComponent<Animator>();
        anim.SetInteger("State", GetState());
        int random = Random.Range(0, 3);
        anim.SetInteger("AnimationId", random);
        anim.SetTrigger("GoToState");
    }
    int GetState()
    {
        return GameplayController.Instance.GetScreenState();
    }
    public void RemoveSpaceship(EnemyController enemy)
    {
        if(spawnedEnemies.Contains(enemy))
        spawnedEnemies.Remove(enemy);
    }
    public void DestroyAllSpaceship()
    {
        
        foreach(EnemyController enemy in spawnedEnemies)
        {
            Destroy(enemy.gameObject);
        }
        spawnedEnemies.Clear();
    }


}
