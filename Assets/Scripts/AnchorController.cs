﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnchorController : MonoBehaviour {

    private Rigidbody anchored;
    private FixedJoint joint;

	// Use this for initialization
	void Start () {

        joint = GetComponent<FixedJoint>();
        anchored = joint.connectedBody;
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = new Vector3(transform.position.x, anchored.transform.position.y, transform.position.z);
	}
}
