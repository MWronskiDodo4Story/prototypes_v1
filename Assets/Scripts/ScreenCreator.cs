﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenCreator : MonoBehaviour {

    [SerializeField]
    private ScreenController screen;
    [SerializeField]
    private Transform mainScreen;
    [SerializeField]
    private Transform reflectionProbe;

    public int amount;

	// Use this for initialization
	void Start () {
		for (int i=0; i<amount; i++)
        {
            ScreenController newScreen = Instantiate(screen, mainScreen);
            float rotation = i * (360/(amount * 1f));
            newScreen.transform.Rotate(Vector3.up, rotation);
            newScreen.UpdateScreenWidth(2*Mathf.PI*5 / amount);
            newScreen.SetupAnchorOverride(reflectionProbe);
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
