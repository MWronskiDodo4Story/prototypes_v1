﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MessageDisplay : MonoBehaviour {

    [SerializeField]
    private TextMeshPro text;
    public static MessageDisplay Instance;
    private Coroutine currentDisplayCoroutine = null;
    private Coroutine currentPulseCoroutine = null;
    // Use this for initialization
    void Start () {
        if (Instance == null) Instance = this;
	}
	
	// Update is called once per frame
	void Update () {

    }
    public void HideAllText()
    {
        text.color = Color.red;
        StopAllCoroutines();
        text.gameObject.SetActive(false);

    }
    public void ShowText(string t, float fontSize=100)
    {
        text.gameObject.SetActive(true);
        if (currentPulseCoroutine==null)
        {
            if (currentDisplayCoroutine != null)
            {
                StopCoroutine(currentDisplayCoroutine);
            }
            text.fontSize = fontSize;
            text.text = t;
            currentDisplayCoroutine = StartCoroutine(Display(1, 1));

        }
    }
    public void ShowTextPulse(string t, float fontSize = 100, float time=0)
    {
        StopAllCoroutines();
        text.fontSize = fontSize;
        text.text = t;
        if(time==0)
        {

            currentPulseCoroutine =  StartCoroutine(DisplayWithPulse(0.4f));
        }
        else
        {
            currentPulseCoroutine = StartCoroutine(DisplayWithPulse(0.4f, time));
        }

    }
    public void ShowTextNoFadeout(string t, float fontSize = 100)
    {
        StopAllCoroutines();
        text.alpha = 1;
        text.fontSize = fontSize;
        text.gameObject.SetActive(true);
        text.text = t;

    }
    public void ShowTextNoFadeout(string t, Color c, float fontSize = 100)
    {
        StopAllCoroutines();
        text.alpha = 1;
        text.color = c;
        text.fontSize = fontSize;
        text.gameObject.SetActive(true);
        text.text = t;

    }
    IEnumerator Display(float time, float delay)
    {
        text.alpha = 1;
        text.gameObject.SetActive(true);
        yield return new WaitForSeconds(delay);
        float startTime = Time.time;
        while (startTime + time > Time.time)
        {
            yield return new WaitForEndOfFrame();
            text.alpha = 1 - (1 * (Time.time - startTime));
        }
        text.gameObject.SetActive(false);
    }
    IEnumerator DisplayWithPulse(float pulse)
    {
        text.gameObject.SetActive(true);
        float startTime = Time.time;
        bool direction = true;
        while (true)
        {
            yield return new WaitForEndOfFrame();
            if (direction)
            {
                text.alpha = 1 - ((1 / pulse) * (Time.time - startTime));
            }
            else
            {
                text.alpha = (1 * (1 / pulse) * (Time.time - startTime));
            }
            if (direction && text.alpha < 0.1f)
            {
                direction = false;
                startTime = Time.time;
            }
            else
            if (!direction && text.alpha > 0.9f)
            {
                direction = true;
                startTime = Time.time;
            }
        }
    }
    IEnumerator DisplayWithPulse(float pulse, float time)
    {
        
        text.gameObject.SetActive(true);
        float startTime = Time.time;
        float startTime1 = Time.time;
        bool direction = true;
        while (startTime1+time>Time.time)
        {
            yield return new WaitForEndOfFrame();
            if (direction)
            {
                text.alpha = 1 - ((1 / pulse) * (Time.time - startTime));
            }
            else
            {
                text.alpha = (1 * (1 / pulse) * (Time.time - startTime));
            }
            if (direction && text.alpha < 0.1f)
            {
                direction = false;
                startTime = Time.time;
            }
            else
            if (!direction && text.alpha > 0.9f)
            {
                direction = true;
                startTime = Time.time;
            }

        }
        text.gameObject.SetActive(false);
    }
}
