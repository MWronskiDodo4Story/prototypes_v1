﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class HealthDisplay : MonoBehaviour {

    [SerializeField]
    private TextMeshPro text;
    public static HealthDisplay Instance;
	// Use this for initialization
	void Start () {
        if (Instance == null) Instance = this;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void ShowText(string t)
    {
        StopAllCoroutines();
        text.text = t;
        StartCoroutine(Display(1, 1));

    }
    IEnumerator Display(float time, float delay)
    {
        text.gameObject.SetActive(true);
        float startTime = Time.time;
        while(startTime+time+delay>Time.time)
        {
            yield return new WaitForEndOfFrame();
            text.alpha = delay+1 - (1 * (Time.time-startTime));
        }
        text.gameObject.SetActive(false);
    }
}
