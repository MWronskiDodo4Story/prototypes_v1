﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class GameOverScreen : MonoBehaviour {

    [SerializeField]
    private TextMeshPro score;
    [SerializeField]
    private TextMeshPro time;

	// Use this for initialization
	void Start () {
        UpdateText();
	}
    private void OnEnable()
    {
        UpdateText();
    }

    void UpdateText()
    {
        UpdateScore();
        UpdateTime();
    }
    void UpdateScore()
    {
        score.text = "Your score: " + GameplayController.Instance.GetPoints() + " Points";
    }
    void UpdateTime()
    {
        time.text = "Time: " + (int)GameplayController.Instance.GetTime() + " Seconds";
    }
}
