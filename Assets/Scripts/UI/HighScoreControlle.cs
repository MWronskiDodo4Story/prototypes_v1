﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class HighScoreControlle : MonoBehaviour {

    [SerializeField]
    private TextMeshPro p1;
    [SerializeField]
    private TextMeshPro p2;
    [SerializeField]
    private TextMeshPro p3;
    [SerializeField]
    private TextMeshPro p4;
    [SerializeField]
    private TextMeshPro p5;

    private void OnEnable()
    {
        SetScores();
    }
    void SetScores()
    {
        p1.text = "You: " + GameplayController.Instance.GetPoints() + " points";
        p2.text = "Player 2: " + (int)(GameplayController.Instance.GetPoints()/1.5) + " points";
        p3.text = "Player 3: " + (int)(GameplayController.Instance.GetPoints()/2.5) + " points";
        p4.text = "Player 4: " + (int)(GameplayController.Instance.GetPoints()/4) + " points";
        p5.text = "Player 5: " + 0 + " points";
    }

}
