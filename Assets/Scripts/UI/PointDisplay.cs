﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class PointDisplay : MonoBehaviour {

    private TextMeshPro text;
	void Start () {
        text = GetComponent<TextMeshPro>();
        Animate(1, 1);
	}

    IEnumerator FadeOut(float time, float delay)
    {
        float startTime = Time.time;
        while (startTime + time + delay > Time.time)
        {
            yield return new WaitForEndOfFrame();
            text.alpha = delay + 1 - (1 * (Time.time - startTime));
        }
    }
    IEnumerator FloatAway(float time)
    {
        float startTime = Time.time;
        while (startTime + time > Time.time)
        {
            yield return new WaitForEndOfFrame();
            transform.position += new Vector3(0, 0.05f, 0);
        }
        Destroy(this.gameObject);
    }
    void Animate(float time, float delay)
    {
        ChangeComboGraphic();
        StartCoroutine(FadeOut(time, delay));
        StartCoroutine(FloatAway(time + delay));
    }
    void ChangeComboGraphic()
    {
        float combo = GameplayController.Instance.GetCurrentComboMultiplier();
        if(combo>2)
        {
            Color c = new Color(text.color.r-combo/5, text.color.g, text.color.b-combo/5);
            text.color = c;
            text.fontSize = text.fontSize + text.fontSize * (combo / 10);
        }
    }
}
