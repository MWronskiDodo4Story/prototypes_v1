﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SceneMovement : MonoBehaviour
{
    [SerializeField]
    private Transform scene;
    [SerializeField]
    private Transform shootingProbes;


    public Transform followObject;

    public bool mouseControls;
    public bool padControls;
    public float speed;

    public static SceneMovement Instance;

    private int playerSector;
    private bool initialAngleSet = false;
    private float initialAngle = 0;
    private float currentAngle = 0;
    // Use this for initialization
    void Start () {
        if (Instance == null)
        {
            Instance = this;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if(followObject==null)
        {
            if (mouseControls) { MouseMovement(); }
            if (padControls) { PadMovement(); }
        }
        else
        {
            FollowObject(true);
        }
    }
    #region ControlMethods
    void MouseMovement()
    {
        if (Input.GetAxis("Horizontal") > 0)
        {
            scene.Rotate(Vector3.up, speed);
        }
        if (Input.GetAxis("Horizontal") < 0)
        {
            scene.Rotate(Vector3.up, -speed);
        }
    }
    void PadMovement()
    {
        if (Input.GetAxis("PadCrossX") > 0)
        {
            scene.Rotate(Vector3.up, speed);
            shootingProbes.Rotate(Vector3.up, speed);
        }
        if (Input.GetAxis("PadCrossX") < 0)
        {
            scene.Rotate(Vector3.up, -speed);
            shootingProbes.Rotate(Vector3.up, -speed);
        }
        GameplayController.Instance.SetScreenState(scene.rotation.eulerAngles.y);
    }
    #endregion

    int GetSector(float angle)
    {
        if(angle<0)
        {
            angle = 180 - angle;
        }
        int sector = (int)((angle + 20) / 40);
        return sector;
    }
    public int GetSector(Transform tr)
    {
        Vector3 distanceV = (new Vector3(tr.position.x, 0, tr.position.z) - new Vector3(shootingProbes.position.x, 0, shootingProbes.position.y)).normalized;
        Vector3 worldV = Vector3.back;
        return GetSector(Vector3.SignedAngle(distanceV, worldV, Vector3.up));
    }
    public int GetPlayerSector()
    {
        return playerSector;
    }
    public void Rotate(bool right)
    {
        if (right)
        {
            scene.Rotate(Vector3.up, speed);
            shootingProbes.Rotate(Vector3.up, speed);
        }
        else
        {
            scene.Rotate(Vector3.up, -speed);
            shootingProbes.Rotate(Vector3.up, -speed);
        }
    }
    private float CheckAngle()
    {
        Vector3 distanceV = (new Vector3(followObject.position.x, 0, followObject.position.z) - new Vector3(shootingProbes.position.x, 0, shootingProbes.position.y)).normalized;
        Vector3 worldV = Vector3.back;
        if (!initialAngleSet)
        {
            initialAngle = Vector3.SignedAngle(distanceV, worldV, Vector3.up);
            playerSector = GetSector(initialAngle);
            currentAngle = initialAngle;
            initialAngleSet = true;
        }
        return Vector3.SignedAngle(distanceV, worldV, Vector3.up);
    }
    public void FollowObject(bool disableControls)
    {
        float a = CheckAngle();
        if (followObject == null)
        {
            return;
        }
        
        shootingProbes.LookAt(followObject);
        Vector3 eulers = shootingProbes.eulerAngles;
        scene.eulerAngles = new Vector3(0, eulers.y+180, 0);
        Vector3 direction =  new Vector3(shootingProbes.transform.position.x, 0, shootingProbes.transform.position.z) - new Vector3( followObject.transform.position.x, 0, followObject.transform.position.z);
       
    }
    public void FollowObjectProgressively(bool disableControls)
    {
        float a = CheckAngle();
        if (followObject == null)
        {
            return;
        }
        if(recentering)
        {
            return;
        }
        float c = currentAngle + 30;

        if (a > currentAngle + 30)
        {
            StartCoroutine(RecenterScreen(40, -1.2f));
        }
        if (a < currentAngle - 30)
        {
            StartCoroutine(RecenterScreen(-40, 1.2f));
        }
    }
    bool recentering = false;
    IEnumerator RecenterScreen(float angle, float speed)
    {
        Debug.Log(currentAngle + angle);
        recentering = true;
        if(currentAngle+angle<180&&currentAngle+angle>-180)
        {
            currentAngle += angle;
        }
        else
        {
            if(angle>0)
            {
                currentAngle = -180+angle - (180 - currentAngle);
            }
            else
            {
                currentAngle = 180-(180 + currentAngle)+angle;
            }
        }
        float startTime = Time.time;
        float i = 0;
        while(i<Mathf.Abs(angle))
        {
            scene.Rotate(scene.up, speed);
            i += Mathf.Abs(speed);
            yield return new WaitForFixedUpdate();
        }
        recentering = false;
    }
    public Vector3 GetCurrentRotationEuler()
    {
        return scene.eulerAngles;
    }
    public Quaternion GetCurrentRotationQuaternion()
    {
        return scene.rotation;
    }
}
