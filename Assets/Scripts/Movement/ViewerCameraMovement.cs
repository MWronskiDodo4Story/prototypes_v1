﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ViewerCameraMovement : MonoBehaviour
{
    public bool lockMovement;
    public bool mouseControls;
    public bool padControls;

    [Range(0, 90)]
    public float maxAngleX;
    [Range(0, 90)]
    public float maxAngleY;
    [Range(0.5f, 2.5f)]
    public float speed;

    [Range(0, 1f)]
    public float sensitivity;

    [SerializeField]
    private Transform viewer;
    [SerializeField]
    private Transform viewerXAxis;
    [SerializeField]
    private Transform platform;

    // Use this for initialization
    void Start()
    {
        //viewer.rotation.Set(0, 0, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if(!lockMovement)
        {
            if (padControls) { PadControls(); }
            if (mouseControls) { MouseControls(); }

        }

    }
    #region ControlMethods
    void MouseControls()
    {
        if (Input.GetAxis("Mouse X") > 0)
        {
            if (viewerXAxis.localRotation.y < maxAngleX / 90)
                viewerXAxis.Rotate(Vector3.up, Input.GetAxis("Mouse X")*speed);
        }
        if (Input.GetAxis("Mouse X") < 0)
        {
            if (viewerXAxis.localRotation.y > -maxAngleX / 90)
                viewerXAxis.Rotate(Vector3.up, Input.GetAxis("Mouse X") * speed);
        }
        if (Input.GetAxis("Mouse Y") > 0)
        {

            if (viewer.localRotation.x > -maxAngleY / 90)
                viewer.Rotate(Vector3.left, Input.GetAxis("Mouse Y") * speed);
        }
        if (Input.GetAxis("Mouse Y") < 0)
        {
            if (viewer.localRotation.x < maxAngleY / 90)
                viewer.Rotate(Vector3.left, Input.GetAxis("Mouse Y") * speed);
        }
    }
    void PadControls()
    {
        if (Input.GetAxis("PadRightStickX") > sensitivity)
        {
            if (viewerXAxis.localRotation.y < maxAngleX / 90)
                viewerXAxis.Rotate(Vector3.up, Input.GetAxis("PadRightStickX") * speed);
        }
        if (Input.GetAxis("PadRightStickX") < -sensitivity)
        {
            if (viewerXAxis.localRotation.y > -maxAngleX / 90)
                viewerXAxis.Rotate(Vector3.up, Input.GetAxis("PadRightStickX") * speed);
        }
        if (Input.GetAxis("PadRightStickY") > sensitivity)
        {
            //plus
            if (viewer.localRotation.x < maxAngleY / 90)
                viewer.Rotate(Vector3.left, -Input.GetAxis("PadRightStickY") * speed);
        }
        if (Input.GetAxis("PadRightStickY") < -sensitivity)
        {
            if (viewer.localRotation.x > -maxAngleY / 90)
                viewer.Rotate(Vector3.left, -Input.GetAxis("PadRightStickY") * speed);
        }
    }
    #endregion
}
