﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossHairMovement : MonoBehaviour {

    [Range(0, 1f)]
    public float sensitivity;
    [Range(0f, 2.5f)]
    public float speed;
    [Range(0f, 1f)]
    [Tooltip("0.24 is optimal for full screen width")]
    public float maxAngleX = 0.24f;
    [Range(0f, 0.020f)]
    [Tooltip("0.008 is optimal for full screen height")]
    public float maxPositionY = 0.008f;

    [SerializeField]
    private Transform crosshair;
    [SerializeField]
    private Transform crosshairAnchorX;


    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        //if (GameplayController.Instance.isGameStarted())
            MoveCrossHair();
	}
    void MoveCrossHair()
    {
        if (Input.GetAxis("PadLeftStickX") > sensitivity)
        {
            if (crosshairAnchorX.localRotation.y >= maxAngleX)
            {
                SceneMovement.Instance.Rotate(true);
            }
            else
            crosshairAnchorX.Rotate(Vector3.forward, Input.GetAxis("PadLeftStickX") * speed);
            
        }
        if (Input.GetAxis("PadLeftStickX") < -sensitivity)
        {
            if (crosshairAnchorX.localRotation.y <= -maxAngleX)
            {
                SceneMovement.Instance.Rotate(false);
            }
            else
            crosshairAnchorX.Rotate(Vector3.forward, Input.GetAxis("PadLeftStickX")*speed);
        }
        if (Input.GetAxis("PadLeftStickY") > sensitivity)
        {
            if (crosshair.localPosition.z>-maxPositionY)
            crosshair.Translate(0,  -Input.GetAxis("PadLeftStickY")*speed/10, 0); 
        }
        if (Input.GetAxis("PadLeftStickY") < -sensitivity)
        {
            if (crosshair.localPosition.z < maxPositionY)
                crosshair.Translate(0,  -Input.GetAxis("PadLeftStickY")*speed/10, 0);
        }
    }
}
