﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneAnimationEvents : MonoBehaviour {

    [SerializeField]
    private PlaneController pc;

    public void RotationStarted()
    {
        pc.RotationStarted();
    }
    public void RotationEnded()
    {
        pc.RotationEnded();
    }
}
